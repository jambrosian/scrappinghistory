import scrap
import data_handling

# Automatic scrap
scrap.scrap1('https://www.bstu.de/en/international/european-network/', "Federal Commissioner for Records of the the State Security Service (BStU)", "article-content is-narrow", "./data/temp_links.csv")

# Manual scrap
# Use: (links_link, name, file with links)
#scrap.scrap_man('http://www.muzeum-ak.pl/polecamy/index.php', 'Muzeum Armii Krajowej', "./data/manual_links.csv", "./data/temp_links.csv")

# Add point as a vertex
# Use when cannot figure out links site, or links link to the same page.
#scrap.vertex_only('https://muzeum1939.pl/','Museum of the Second World War in Gdansk')

# To mine the 599 links of the Gedenkstatten Forum
#for x in range(5, 28):
#    scrap.scrap_geden('https://www.gedenkstaettenforum.de/nc/linksammlung/browse/' + str(x) + "/", "Topography of Terror Berlin", "news-list-item", "./data/temp_links.csv")


# Delete unnecessary links. 
#data_handling.del_links("bad_links")

# Scrap for Lern- und Gedenkort Schloss Hartheim
#scrap.scrap_lern('http://www.doew.at/links/gedenkstaetten', "Dokumentationsarchiv des österreichischen Widerstandes", "elements", "./data/temp_links.csv")