## Scrapping History

This project involves the collection and analysis of online connections between different memory organizations in Europe, specifically, those organizations that seek to showcase the effects of Nazism and communism. Online connections are defined as links displayed through a partners or collaborators page on an organization's website. 

The manual collection of online links is very laborious and time-consuming, especially when an organization has dozens of partners. To diminish the collection time and increase the number of links collected, this project makes use of Python scripts that automatically "scrap" the links from a website. 



