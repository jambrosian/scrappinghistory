# These functions will handle adding and removing data
# from the edges and vertices files. 
import csv
import urllib.parse as urlparse

# Add website to list of vertices.
def add_vertex(url, name):
  # Open the vertices files in read mode. 
  # r+ Open for reading and writing. Stream is positioned
  #    at the beginning of the file.
  # a+ Open for reading and appending (writing at the end of file). 
  #    The file is created if it does not exist. 
  with open('./data/vertices.csv', 'r') as v:
    reader = csv.reader(v)
    vertices = list(reader) 
    found_vertex = False 
    # Check if the vertex already exists. 
    for vertex in vertices:
      parsed_url = urlparse.urlparse(url).hostname 
      if (vertex[0] == parsed_url):
        # Increase its popularity by one. 
        vertex[3] = str(int(vertex[3]) + 1)
        found_vertex = True
        break
    v.close()
    with open('./data/vertices.csv', 'w') as v1:
      writer = csv.writer(v1)
      writer.writerows(vertices)
      if not found_vertex:
        print ("Adding new vertex")
        root_url = urlparse.urlparse(url).hostname
        print ("Root url: ", root_url)
        # We are not going to add country, we are going to 
        # add it in a separate file to use in Python and R. 
        popularity = '0'  # No one has pointed at them yet. 
        # with open('./data/vertices.csv', 'w') as v1:
        writer.writerow([root_url, url, name, popularity])

# list_edges[parent_link, child_link]
def add_edges(list_edges):
  # set from and to as the root of the websites.
  # check if a link already exists. 
  with open('./data/edges.csv', 'a+') as e:
    reader = csv.reader(e)
    edges = list(reader)
    writer = csv.writer(e)
    #print ("These are the edges")
    for edge in list_edges:
      print (edge)
      if edge not in edges:
        # This is in case urlparse returns NoneType 
        link = urlparse.urlparse(edge[1]).hostname or edge[1]   
        #print ("This is the link %s" % link)
        writer.writerow([edge[0], link])

# Delete links that are not relevant to this project.
def del_links(bad_links_rows):
  # print(bad_links_list)
  # Get a list of all the rows to be deleted. 
  row_num_bad_links = []

  with open(bad_links_rows, 'r') as a:
    # .strip() -> Removes all leading and trailing whitespace. 
    # .rstrip('\'n') -> Removes trailing. '\n'
    # Quick way to do it, but we'll need each line since we are 
    # going to get the root link. 
    # row_num_bad_links = [int(line.strip()) for line in a]
    for line in a:
      index = line.strip()
      if '-' in index:
         left_index = int(index[:index.find('-')])
         right_index = int(index[index.find('-')+1:])
         for x in range(left_index, right_index+1):
              row_num_bad_links.append(x)
      else:
        row_num_bad_links.append(int(index))
  # Make a new temp file with all just the approved files.
  # Save the root url of the bad links to delete them in the edges. 
  bad_links_dict = {}
  good_links_list = []
  good_links_dict = {}
  #print (row_num_bad_links)
  with open("./data/temp_links.csv") as b:
    reader = csv.reader(b)
    for row in reader:
      #print (row)
      print ('line number: ', reader.line_num)
      if (len(row) != 3):
        print (row)
        print ("Length of row less than 3")
        continue
      key = urlparse.urlparse(row[1]).hostname or row[1]
      
      if (reader.line_num == 224):
            print ('key for 224: ', key)
      #print ("row number: ", reader.line_num)
      if reader.line_num == row_num_bad_links[0]:
            if key not in good_links_dict:
              bad_links_dict[key] = 1
              print ('row: ', reader.line_num, ' url: ', key)
            row_num_bad_links.pop(0)
      else:
            good_links_list.append(row)
            good_links_dict[key] = reader.line_num
      if not row_num_bad_links:
            break
  # Make the new file for temp links. 
  with open("./data/temp_links_update.csv", 'w') as c:
    writer = csv.writer(c)
    writer.writerows(good_links_list)
  # Delete the bad links from the edges file. 
  old_edges = []
  with open("./data/edges.csv", 'r') as d:
    reader = csv.reader(d)
    old_edges = list(reader) 
  with open("./data/updated_edges.csv", 'w') as e:
    writer = csv.writer(e)
    for edge in old_edges:
      if edge[1] not in bad_links_dict:
        writer.writerow(edge)
  
