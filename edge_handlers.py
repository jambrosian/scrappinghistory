# These functions maintain the edges.csv files
# and any file that was derived from it. 

import csv
import urllib.parse as urlparse

# Delete links that are not relevant to this project.
def del_links(bad_links_rows):
  # print(bad_links_list)
  # Get a list of all the rows to be deleted. 
  row_num_bad_links = []

  with open(bad_links_rows, 'r') as a:
    # .strip() -> Removes all leading and trailing whitespace.
    # .rstrip('\'n') -> Removes trailing. '\n'
    # Quick way to do it, but we'll need each line since we are
    # going to get the root link.
    # row_num_bad_links = [int(line.strip()) for line in a]
    for line in a:
      index = line.strip()
      if '-' in index:
         left_index = int(index[:index.find('-')])
         right_index = int(index[index.find('-')+1:])
         for x in range(left_index, right_index+1):
              row_num_bad_links.append(x)
      else:
        row_num_bad_links.append(int(index))
  # Make a new temp file with all just the approved files.
  # Save the root url of the bad links to delete them in the edges.
  bad_links_dict = {}
  good_links_list = []
  good_links_dict = {}
  #print (row_num_bad_links)
  with open("./data/temp_links.csv") as b:
    reader = csv.reader(b)
    for row in reader:
      #print (row)
      print ('line number: ', reader.line_num)
      if (len(row) != 3):
        print (row)
        print ("Length of row less than 3")
        continue
      key = urlparse.urlparse(row[1]).hostname or row[1]
      
      if (reader.line_num == 224):
            print ('key for 224: ', key)
      #print ("row number: ", reader.line_num)
      if reader.line_num == row_num_bad_links[0]:
            if key not in good_links_dict:
              bad_links_dict[key] = 1
              print ('row: ', reader.line_num, ' url: ', key)
            row_num_bad_links.pop(0)
      else:
            good_links_list.append(row)
            good_links_dict[key] = reader.line_num
      if not row_num_bad_links:
            break
  # Make the new file for temp links. 
  with open("./data/temp_links_update.csv", 'w') as c:   # Make notes regarding this.
    writer = csv.writer(c)
    writer.writerows(good_links_list)
  # Delete the bad links from the edges file. 
  old_edges = []
  with open("./data/edges.csv", 'r') as d:
    reader = csv.reader(d)
    old_edges = list(reader) 
  with open("./data/updated_edges.csv", 'w') as e:
    writer = csv.writer(e)
    for edge in old_edges:
      if edge[1] not in bad_links_dict:
        writer.writerow(edge)
  


def valid_vertex_only(edges_file, vertices_file, new_edges_file):
    '''
    This function deletes the edges that do not have a 'to' value that
    is a valid vertex and outputs them in the ./data directory
    under the name specified in new_edges_file. A valid vertex is 
    defined as a link that can be found in the vertices_file. 
    In order to create a graph with the resulting edges and vertices 
    in R, the edges with a valid vertex for 'from', are added with the 
    format ['edge', 'edge']. Duplicates are handled in R. 
    '''
    vertices = {}
    with open(vertices_file, 'r') as v:
        reader = csv.DictReader(v)
        for vertex in reader:
            vertices[vertex['root']] = 0
    with open(edges_file, 'r') as e:
        reader = csv.DictReader(e)
        with open(new_edges_file, 'w') as ne:
            writer = csv.writer(ne)
            writer.writerow(["from","to"])
            for edge in reader:
                if (edge['from'] in vertices and edge['to'] not in vertices):
                    writer.writerow([edge['from'], edge['from']])
                elif (edge['to'] in vertices and edge['from'] in vertices):
                    writer.writerow([edge['from'], edge['to']])

# Calling the function. 
valid_vertex_only('./data/edges.csv', './data/new_vertices_pop.csv', './data/edges_r.csv')

# How to run this script:
# python edge_handlers.py 

