# This code will scrap the pages for one website
# given the link to the specific page to be scrapped.

# The following code was used to scrap 
# http://www.majdanek.eu/en given the link that 
# contains all its partners. 

import csv 
import requests
from bs4 import BeautifulSoup

f = csv.writer(open('hdke-links.csv', 'w'))
f.writerow(['Name', 'Link'])

url = 'http://hdke.hu/en/partners-sponsors'
page = requests.get(url)

soup = BeautifulSoup(page.text, 'html.parser')

links_list = soup.find(class_="article-entry")

links_list_items = links_list.find_all('p')[1:]

for link in links_list_items:
  orgName = link.contents[0]
  print (orgName) 
  if (orgName == u'\xa0'):
    continue
  if (isinstance(orgName, str)):
    links = link.get('href')
    f.writerow([orgName, links])
  else:
    break