# This code will scrap the pages for one website
# given the link to the specific page to be scrapped.

# The following code was used to scrap 
# https://www.kz-gedenkstaette-dachau.de/links-d.html
# given the link that contains all its partners. 

import csv
import requests
from bs4 import BeautifulSoup

f = csv.writer(open('kz-links.csv','w'))
f.writerow(['Name', 'Link'])

url = 'https://www.kz-gedenkstaette-dachau.de/links-d.html'
page = requests.get(url)

soup = BeautifulSoup(page.text, 'html.parser')

links_list = soup.find(id="links-94")
links_list_items = links_list.find_all('a')

for link in links_list_items:
  orgName = link.contents[0]
  links = link.get('href')

  f.writerow([orgName, links])