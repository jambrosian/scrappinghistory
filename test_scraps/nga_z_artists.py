# Import libraries
import csv
import requests
from bs4 import BeautifulSoup


f = csv.writer(open('z-artist-names.csv', 'w'))
f.writerow(['Name', 'Link'])

pages = []

for i in range(1,5):
	url = 'https://web.archive.org/web/20121007172955/https://www.nga.gov/collection/anZ' + str(i) + '.htm'
	pages.append(url)

for item in pages:
	# Collect first page of artists' list
	page = requests.get(item)

	# Create a BeautifulSoup object by parsing the page content
	# with python's html.parser
	soup = BeautifulSoup(page.text, 'html.parser')
	#print ("This is the soup")
	#print (soup)

	# Remove bottom links from soup through the decompose method. 
	last_links = soup.find(class_='AlphaNav')
	last_links.decompose()

	# Pull al text from BodyText div
	artist_name_list = soup.find(class_='BodyText')
	#print ("this is the list")
	#print (artists_name_list)


	# Pull text from all instances of <a> tag within BodyText div
	artist_name_list_items = artist_name_list.find_all('a')
	#print ("this is the list of artists names")
	#print (artist_name_list_items)


	# Create for loop to print out all artists' names
	for artist_name in artist_name_list_items:
	#	print (artist_name.content)
		names = artist_name.contents[0]
		links = 'https://web.archive.org' + artist_name.get('href')
		
		# Add each artist's name and associated link to a row
		f.writerow([names,links])
