# This code will scrap the pages for one website
# given the link to the specific page to be scrapped.

# The following code was used to scrap 
# http://www.gedenkstaette-breitenau.de/ given 
# the link that contains all its partners. 

import csv
import requests
from bs4 import BeautifulSoup

f = csv.writer(open('gedenk-links.csv', 'w'))
f.writerow(['Name', 'Link'])

url = 'http://www.gedenkstaette-breitenau.de/links.htm'
page = requests.get(url)

soup = BeautifulSoup(page.text, 'html.parser')
#print(soup)

#tables = soup.findAll('tr')
#print(tables[1].findAll('td')[2])
#links_list = soup.find_all('a', {'target' : '_blank'})
#print (links_list)

links_list = soup.findAll('tr')[1].findAll('td')[2].findAll('a')
print (links_list)

# print(len(links_list[1].find('b').contents))
# orgName = links_list[0].find('b').contents[0]
# orgName2 = links_list[0].find('b').contents[1].contents[0]
# print("ORGANIZATION NAME")
# print(orgName+orgName2)
# link = links_list[0].get('href')
# print("LINK")
# print(link)

for link in links_list:
  # print("link.find('b')")
  # print(link.find('b'))
  # print("link.find('b').contents[0]")
  # print(link.find('b').contents[0])
  orgName = link.contents[0]
  if (link.find('b') != None):
    orgName = link.find('b').contents[0] 
    if (len(links_list[1].find('b').contents) > 1):
      orgName2 = link.find('b').contents[1].contents[0]
      orgName = orgName + orgName2

  links = link.get('href')

  f.writerow([orgName, links])



