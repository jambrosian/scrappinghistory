# This code will scrap the pages for one website
# given the link to the specific page to be scrapped.

# The following code was used to scrap 
# http://www.auschwitz.org/ given the link that 
# contains all its partners. 

# Import libraries
import csv
import requests
from bs4 import BeautifulSoup

f = csv.writer(open('partner-links.csv', 'w'))
f.writerow(['Name', 'Link'])

url = 'http://auschwitz.org/en/links/local-partners/'
page = requests.get(url)

soup = BeautifulSoup(page.text, 'html.parser')
#print ("This is the soup")
#print (soup)

# Look for the div that has the data.
links_list = soup.find(class_="desc-module")
# print("This is the list of links")
# print(links_list)

# Pull the text from all the instances with the <a> tag.
# This will return a list with all the <a> tags found. 
links_list_items = links_list.find_all('a')
print("This is the list of links")
print(links_list_items)

# Create for loop to print out all artists' names
for link in links_list_items:
  orgName = link.contents[0]
  links = link.get('href')

  # Add each link to the list. 
  f.writerow([orgName,links])

