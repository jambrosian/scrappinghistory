# This code will scrap the pages for one website
# given the link to the specific page to be scrapped.

# The following code was used to scrap 
# https://www.iiccr.ro/en/about-us/partners/ and its
# sublinks.

import csv
import requests
from bs4 import BeautifulSoup

f = csv.writer(open('iiccr-links.csv','w'))
f.writerow(['Name', 'Link'])

url = 'http://www.iiccr.ro/en/event-partnerships/'
page = requests.get(url)

soup = BeautifulSoup(page.text, 'html.parser')
list_links = soup.find(class_="entry-content")
list_links_items = list_links.find_all('a')

for link in list_links_items:
  print (link.contents)
  orgName = link.contents[0]
  links = link.get('href')

  f.writerow([orgName, links])
