# This code will scrap the pages for one website
# given the link to the specific page to be scrapped.

# The following code was used to scrap 
# http://old.ustrcr.cz/en/links given the link that 
# contains all its partners. 

import csv 
import requests

from bs4 import BeautifulSoup

f = csv.writer(open('old-links.csv', 'w'))
f.writerow(['Name', 'Link'])

url = 'http://old.ustrcr.cz/en/links'
page = requests.get(url)

soup = BeautifulSoup(page.text, 'html.parser')

links_list = soup.find(id="content")
links_list_items = links_list.find_all('a')

print(links_list_items)

for link in links_list_items:
  orgName = link.contents[0]
  links = link.get('href')
  f.writerow([orgName, links])
