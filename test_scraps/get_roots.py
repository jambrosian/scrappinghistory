
import csv
import urllib.parse as urlparse


csvfile = open('./data/LinkList.csv', 'r')
reader = csv.DictReader(csvfile)

new_csvfile = open('./data/LinksList_2.csv', 'w')

# What exactly does the with do?
with csvfile and new_csvfile:
  header = ["id", "link", "root", "name", "topic", "country", "popularity", "scrapped"]
  writer = csv.DictWriter(new_csvfile, fieldnames=header, extrasaction='ignore', delimiter=',')
  writer.writeheader()

  for row in reader:
    row["root"] = urlparse.urlparse(row["root"]).hostname
    writer.writerow(row)



