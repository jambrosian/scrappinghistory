# This file contains functions that will handle the production 
# and deletion of new vertices based on temp_links*.py files. 

import csv
import urllib.parse as urlparse
from data_handling import add_vertex 

def new_vertices(temp_links, new_filename):
    '''
    This function makes a list of new possible vertices based on 
    the temp_links file. If a vertex has already been added to the list,
    then it means that the vertex was referenced by another source. Therefore,
    its popularity is increased by 1. 
    '''
    with open(temp_links, 'r') as v:
        reader = csv.reader(v)
        temp_vertices = list(reader) 
        for vertex in temp_vertices:
            add_vertex(vertex[1], vertex[0], new_filename, True)

# Read temporary links from file temp_links_update.csv in data folder
# and output new possible vertices in the same folder under the name
# new_vertices.csv
# new_vertices('./data/temp_links.csv', './data/new_vertices.csv')


def getPopularity(edges_file, vertices_file, new_vertices_file):
    '''
    This function calculates the popularity (the number of vertices that
    list a link as its partner) of a vertex based on the number
    of edges that reference it ('to'), and outputs the vertices and 
    their popularities in a file with name specified by 
    new_vertices_file. 
    '''
    # Open the csv file for edges into a list. 
    edges = None
    vertices = None
    with open(edges_file, 'r') as e:
        reader = csv.reader(e)
        edges = list(reader)

    # Open the vertices file into a dictionary. 
    with open(vertices_file, 'r') as v:
        reader = csv.reader(v)
        vertices = list(reader)

        # print(vertices[1])
        # Make an actual dictionary with elements in list. 
        vertices_dict = {}
        for x in range(0, len(vertices)):
            vertices_dict[vertices[x][0]] = x
        
        for edge in edges:
            if edge[1] in vertices_dict:
                pop = int(vertices[vertices_dict[edge[1]]][3])
                pop += 1
                vertices[vertices_dict[edge[1]]][3] = str(pop)
        
        with open(new_vertices_file, 'w') as nv:
            writer = csv.writer(nv)
            for row in vertices:
                writer.writerow(row)


# edges_file = './data/updated_edges.csv'
# vertices_file = './data/vertices.csv'
# new_vertices_file = './data/vertices_pop.csv'


# getPopularity(edges_file, vertices_file, new_vertices_file)

def pop_only(vertices, new_vertices_pop, unused_vertices):
    '''
    This function reads in a file of new vertices and creates two 
    new files: new_vertices_pop and unused_vertices. The first file
    consists of new vertices that comply with the requisite of haveing
    a popularity value greater than 2, and the second one is for vertices
    whose popularity is <= 1. 
    '''
    with open(vertices, 'r') as v:
        reader = csv.reader(v)
        vertices = list(reader)
        vertices = vertices[1:]
        with open(new_vertices_pop, 'w') as v1:
            writer_nv = csv.writer(v1)
            writer_nv.writerow(["root","links_page", "name", "popularity"])
            with open(unused_vertices, 'w') as v2:
                writer_uv = csv.writer(v2)
                writer_uv.writerow(["root","links_page", "name", "popularity"])
                for vertex in vertices:
                    if int(vertex[3]) <= 1:
                        writer_uv.writerow(vertex)
                    else:
                        writer_nv.writerow(vertex)

vertices = './data/vertices_pop.csv'
new_vertices_pop = './data/new_vertices_pop.csv'
unused_vertices = './data/unused_vertices_2.csv'

pop_only(vertices, new_vertices_pop, unused_vertices)