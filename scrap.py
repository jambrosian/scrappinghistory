
# Each page has a different version for scrapping,
# but some of them look very similar. 
# So we'll make functions where each one will describe. 

# This is the cleanest type of scrap that we will have.
# Other ones will have to be customized. 
import csv
import requests
from bs4 import BeautifulSoup
from data_handling import *
import urllib.parse as urlparse

# Class site -> CSS class where to find the data. 
def scrap1(url, name, soup_item, output_file):  
  
  # Open list of temporary files. 
  out_f = open(output_file,'a')
  f = csv.writer(out_f) 

  page = requests.get(url)
  soup = BeautifulSoup(page.text, 'html.parser')

  # 1) Class
  list_links = soup.find(class_=soup_item) 
  # 2) ID
  #list_links = soup.find(id=soup_item)

  # Simple, easy way

  list_link_items = list_links.find_all('a')

  # More complex way for complex websites
  # Gedenk
  #list_link_items = soup.findAll('tr')[1].findAll('td')[2].findAll('a')

  list_edges = []
  parent_link = urlparse.urlparse(url).hostname

  for link in list_link_items:
    print (link.contents)
    # Easy painless way
    orgName = link.contents[0]

    # Complex way
    # orgName = link.find('b').contents[0] 

    # Extra information needed (Gedenk)
    # if (link.find('b') != None):
    #   orgName = link.find('b').contents[0] 
    #   if (len(list_link_items[1].find('b').contents) > 1):
    #     orgName2 = link.find('b').contents[1].contents[0]
    #     orgName = orgName + orgName2

    # Immutable
    links = link.get('href')
    list_edges.append([parent_link, links])
    f.writerow([orgName, links, parent_link])
  
  out_f.close()

  # Add website itself to the list of vertices.
  add_vertex(url, name)

  # Add website and its children to list of edges. 
  add_edges(list_edges)

# For this type of scrap, it is just easier to 
# copy paste the links into some file and pass it
# for the program to put the information in the right place.
def scrap_man(url, name, manual_links_file, output_file):
  # Add website itself to the list of vertices. 
  add_vertex(url, name)

  # Add website and its children to list of edges. 
  with open(manual_links_file, 'r') as m:
    reader = csv.reader(m)
    list_edges = list(reader)
    add_edges(list_edges)

    with open(output_file,'a') as temp:
      f = csv.writer(temp)
      for edge in list_edges:
        # Prepend the name of the organization manually
        # since many of these links have the name elsewhere anyway. 
        f.writerow([edge[1], edge[0]])
      
def vertex_only(url, name):
    add_vertex(url, name)#

# This website has a list of over 500 partners: 
# https://www.gedenkstaettenforum.de/nc/linksammlung/
# This function ensures that the links are correctly retrieved. 
def scrap_geden(url, name, soup_item, output_file): 
  # Open list of temporary files. 
  out_f = open(output_file,'a')
  f = csv.writer(out_f) 

  page = requests.get(url)
  soup = BeautifulSoup(page.text, 'html.parser')

  # 1) Class
  list_links = soup.find_all(class_=soup_item) 

  # # 2) ID
  # #list_links = soup.find(id=soup_item)

  # Names and links are separate. 
  list_link_items = []
  list_org_names = []
  for list_link in list_links:
      if list_link.find_all(attrs={"target": "_blank"}) and list_link.find('h2'):
        list_link_items.append(list_link.find_all(attrs={"target": "_blank"})[0])
        list_org_names.append(list_link.find('h2'))

  list_edges = []
  parent_link = urlparse.urlparse(url).hostname
  
  for x in range(0, len(list_link_items)):

      orgName = list_org_names[x].contents[0]
      if list_org_names[x].find('a'):
        orgName = list_org_names[x].find('a').contents[0]

      # Immutable
      print (list_link_items[x])
      links = list_link_items[x].get('href')
      list_edges.append([parent_link, links])
      f.writerow([orgName, links, parent_link])
  
  out_f.close()

  # Add website itself to the list of vertices.
  add_vertex(url, name)

  # Add website and its children to list of edges. 
  add_edges(list_edges)

# Function to scrap: http://www.schloss-hartheim.at/index.php/info/links-und-kooperationspartner
def scrap_lern(url, name, soup_item, output_file):  
  
  # Open list of temporary files. 
  out_f = open(output_file,'a')
  f = csv.writer(out_f) 

  page = requests.get(url)
  soup = BeautifulSoup(page.text, 'html.parser')

  # 1) Class
  list_links = soup.find(class_=soup_item) 

  list_link_items = list_links.find_all('a')
  list_name_items = list_links.find_all(attrs={"style": "width: 365px;"})

  list_edges = []
  parent_link = urlparse.urlparse(url).hostname

  for x in range(0, len(list_link_items)):
    # Easy painless way
    orgName = list_name_items[x].contents[0].contents[0]

    # Immutable
    links = list_link_items[x].get('href')
    list_edges.append([parent_link, links])
    f.writerow([orgName, links, parent_link])
  
  out_f.close()

  # Add website itself to the list of vertices.
  add_vertex(url, name)

  # Add website and its children to list of edges. 
  add_edges(list_edges)