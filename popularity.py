
import csv
import urllib.parse as urlparse

# This file has a function to calculate the popularity of a vertex based on the number
# of edges that reference it ('to'). 

edges_file = './data/unique_edges.csv' # used to be pop_edges.csv
vertices_file = './data/vertices.csv'
new_vertices_file = './data/vertices_pop.csv'

def getPopularity(edges_file, vertices_file, new_vertices_file):
    # Open the csv file for edges into a list. 
    edges = None
    vertices = None
    with open(edges_file, 'r') as e:
        reader = csv.reader(e)
        edges = list(reader)
    # Open the vertices file into a dictionary. 
    with open(vertices_file, 'r') as v:
        reader = csv.reader(v)
        vertices = list(reader)

        print(vertices[1])
        # Make an actual dictionary with elements in list. 
        vertices_dict = {}
        for x in range(0, len(vertices)):
            vertices_dict[vertices[x][0]] = x
        
        for edge in edges:
            if edge[2] in vertices_dict:  # This value might change depending on the file that you use
                print (edge[2])
                pop = int(vertices[vertices_dict[edge[2]]][3])
                pop += 1
                vertices[vertices_dict[edge[2]]][3] = str(pop)
        
        with open(new_vertices_file, 'w') as nv:
            writer = csv.writer(nv)
            for row in vertices:
                writer.writerow(row)

    #  with open('./data/vertices.csv', 'w') as v1:
    #   writer = csv.writer(v1)
    #   writer.writerows(vertices)
    # For each element from edges, check if the 'to' value
    # equals any of the vertices in the dictionary. 
    # If so, then increase its popularity by one. 

getPopularity(edges_file, vertices_file, new_vertices_file)